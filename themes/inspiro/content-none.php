<?php
/**
 * The template part for displaying a message that posts cannot be found.
 */
?>

<section class="no-results not-found">
    <header class="page-header">
    <h1 class="page-title"><?php
        $path =  $_SERVER['REQUEST_URI'];
        //echo $path;
        $pos = strstr($path, "/en/");

        //echo $pos;
        if ($pos) {
             _e( 'Nothing Found', 'wpzoom' );
        }else{
             _e( 'No Hay resultados', 'wpzoom' );
        }
    ?></h1>

    </header><!-- .page-header -->

    <div class="page-content">
        <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

            <p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'wpzoom' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

        <?php elseif ( is_search() ) : ?>
            <p><?php
                $path =  $_SERVER['REQUEST_URI'];
                //echo $path;
                $pos = strstr($path, "/en/");

                //echo $pos;
                if ($pos) {
                    _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wpzoom' );
                }else{
                    _e( 'Lo sentimos, pero nada coincide con sus términos de búsqueda. Por favor, intente nuevamente con algunas palabras clave diferentes.', 'wpzoom' );
                }
            ?></p>
            <?php get_search_form(); ?>

        <?php else : ?>

            <p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'wpzoom' ); ?></p>
            <?php get_search_form(); ?>

        <?php endif; ?>
    </div><!-- .page-content -->
</section><!-- .no-results -->
